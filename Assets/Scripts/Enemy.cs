﻿using UnityEngine;
using System.Collections;


public class Enemy : MonoBehaviour {
    public State posicion;
    //public int minSpeed;            // Velocidad minima del objeto en particular
    //public int maxSpeed;            // Velocidad maxima del objeto en particular
    public int speed;                 // Velocidad actual del objeto en particular
    private Transform shadow;
    

    public enum State {
        cabeza, torax, brazoIzquierdo, brazoDerecho, piernaIzquierda, piernaDerecha
    }
    

    // Se ejecuta al inicio
    void Start () {
        foreach (Transform child in GetComponentInParent<Transform>()) {
            if (child.CompareTag("Shadow")) {
                shadow = child;
            }
        }

         // Controla que posicion le fue asiganada y lo acomoda a la misma
        switch (posicion) {
            case  State.cabeza:
                transform.position = new Vector3(0, 44.5f, transform.position.z);
                shadow.transform.position = transform.position + new Vector3(0, -35, 0);
                break;
            case State.torax:
                transform.position = new Vector3(0, 30, transform.position.z);
                shadow.transform.position = transform.position + new Vector3(0, -30, 0);
                break;
            case State.brazoIzquierdo:
                transform.position = new Vector3(8, 26, transform.position.z);
                shadow.transform.position = transform.position + new Vector3(0, -25, 0);
                break;
            case State.brazoDerecho:
                transform.position = new Vector3(-8, 26, transform.position.z);
                shadow.transform.position = transform.position + new Vector3(0, -25, 0);
                break;
            case State.piernaIzquierda:
                transform.position = new Vector3(4.5f, 7, transform.position.z);
                shadow.transform.position = transform.position + new Vector3(0, -5, 0);
                break;
            case State.piernaDerecha:
                transform.position = new Vector3(-4.5f, 7, transform.position.z);
                shadow.transform.position = transform.position + new Vector3(0, -5, 0);
                break;
        }
        // Avanza hacia el jugador
        //speed = Random.Range(minSpeed, maxSpeed);                       // La velocidad de cada objeto puede ser random
    }

    // Se ejecuta en cada frame
    void Update () {
        transform.Translate(Vector3.back * speed * Time.deltaTime);     // Mueve el objeto hacia la camara
	}

    void OnTriggerEnter (Collider other) {
        if (other.transform.root.CompareTag("Player")) {
            other.GetComponentInParent<Player>().Warning();
        }
    }

    
    // Se ejecuta cuando colisiona con algo
    void OnTriggerExit (Collider other) {
        if (other.transform.root.CompareTag("Player")) {                 // Si el otro objeto es el player
            // Referencia al script 'Player' para evitar tantos 'GetComponent'
            Player player = other.transform.root.GetComponent<Player>();
            // Comprueba que parte del jugador fue golpeada
            switch (other.tag) {
                case "Cabeza":
                    player.HitHead();
                    break;
                case "Torax":
                    player.HitTorax();
                    break;
                case "BrazoIzquierdo":
                    player.HitBrazoIzquierdo();
                    break;
                case "BrazoDerecho":
                    player.HitBrazoDerecho();
                    break;
                case "PiernaIzquierda":
                    player.HitPiernaIzquierda();
                    break;
                case "PiernaDerecha":
                    player.HitPiernaDerecha();
                    break;
            }
            Destroy(gameObject);                                    // Destruye el objeto en particular
        }
    }
}
