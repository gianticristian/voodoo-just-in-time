﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public Animator animator;
    public int life;
    //public BoxCollider[] body;
    

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }

    public int GetLife () {
        return life;
    }

    public void SetLife (int _life) {
        life = _life;
    }

    // Animacion para mostrar una advertencia cuando esta por ser golpeado
    public void Warning () {
        animator.SetTrigger("Warning");
    }

    // Estas son las animaciones para cuando toca el voodoo
    public void ClickHead () {
        animator.SetTrigger("ClickHead");
     
    }

    public void ClickTorax () {
        animator.SetTrigger("ClickTorax");
    }

    public void ClickBrazoIzquierdo () {
        animator.SetTrigger("ClickBrazoIzquierdo");
    }

    public void ClickBrazoDerecho () {
        animator.SetTrigger("ClickBrazoDerecho");
    }

    public void ClickPiernaIzquierda () {
        animator.SetTrigger("ClickPiernaIzquierda");
    }

    public void ClickPiernaDerecha () {
        animator.SetTrigger("ClickPiernaDerecha");
    }

    // Estas son las animaciones para cuando recibe daño
    public void HitHead () {
        animator.SetTrigger("HitHead");
        life--;
    }

    public void HitTorax () {
        animator.SetTrigger("HitTorax");
        life--;
    }

    public void HitBrazoIzquierdo () {
        animator.SetTrigger("HitBrazoIzquierdo");
        life--;
    }

    public void HitBrazoDerecho () {
        animator.SetTrigger("HitBrazoDerecho");
        life--;
    }

    public void HitPiernaIzquierda () {
        animator.SetTrigger("HitPiernaIzquierda");
        life--;
    }

    public void HitPiernaDerecha () {
        animator.SetTrigger("HitPiernaDerecha");
        life--;
    }


    






}
