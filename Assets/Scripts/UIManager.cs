﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public Text hearts;

    public void SetHearts (int _hearts) {
        hearts.text = _hearts.ToString();
    }
}
