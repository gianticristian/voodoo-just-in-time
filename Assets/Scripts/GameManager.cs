﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public UIManager ui;
    public AudioClip menuClip;
    public AudioClip gameClip;
    public Scene nextScene;
    public GameObject enemies;
    public GameObject thePlayer;
    private Player player;
    private AudioSource audioSource;
    private bool playing = false;

    void Awake () {
        audioSource = GetComponent<AudioSource>();
        if (SceneManager.GetActiveScene().name != "MainMenu" && SceneManager.GetActiveScene().name != "GameOver") {
            playing = true;
        }
        else {
            playing = false;
        }
    }

	// Use this for initialization
	void Start () {
        if (playing) {
            player = thePlayer.GetComponent<Player>();
            audioSource.clip = gameClip;
            audioSource.Play();
        }
        else {
            audioSource.clip = menuClip;
            audioSource.Play();
        }

        /*
        if (SceneManager.GetActiveScene().name != "MainMenu" && SceneManager.GetActiveScene().name != "GameOver") {
            player = thePlayer.GetComponent<Player>();
            audioSource.clip = gameClip;
            audioSource.Play();
        }
        else {
            audioSource.clip = menuClip;
            audioSource.Play();
        }
        */

        
	}
	
	// Update is called once per frame
	void Update () {

        if (playing) { 
        //if (SceneManager.GetActiveScene().name != "MainMenu") {
            UpdateUI();
            // Controla si el player perdio
            LooseGame();
            WinGame();

        }


   
	}

    public void StartGame () {
        SceneManager.LoadScene("001");
    }

    public void ExitGame () {
        Application.Quit();
    }

    // Controla si el player tiene vidas o no
    void LooseGame () {
        if (player.GetLife() <= 0) {
            EndGame();
        }
    }

    // Si el player no tiene vidas, finaliza el juego y carga la pantalla final
    void EndGame () {
        SceneManager.LoadScene("GameOver");
    }

    // Controla si gano el level
    void WinGame () {
        if (enemies.GetComponent<Transform>().childCount <= 0) {
            NextLevel();
        }
    }

    // Carga el siguiente nivel
    void NextLevel () {
        SceneManager.LoadScene(nextScene.name);
    }

    void UpdateUI () {
        ui.SetHearts(player.GetLife());
    }

}
